package no.uib.inf101.terminal;
import java.util.HashMap;

public interface Command {
    String run(String[] args);
    String getName();
    String getManual();
    default void setContext(Context context) { /* do nothing */ };
    default void setCommandContext(HashMap <String, Command> comcont) { /* do nothing */ };
}
