package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    
    @Override
    public String run(String[] args) {
        String output = "";
        for (String arg : args) {
            output += arg + " ";
        }
        return output;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
      String instructions = "The echo command prints out whatever text you write after the keyword 'echo'.";
      return instructions;
    }
}
