package no.uib.inf101.terminal;

public class CmdCd implements Command {
    @Override
    public String run(String[] args) {
        if (args.length == 0) {
          this.context.goToHome();
          return "";
        } else if (args.length > 1) {
          return "cd: too many arguments";
        }
        String path = args[0];
        if (this.context.goToPath(path)) {
          return "";
        } else {
          return "cd: no such file or directory: " + path;
        }
    }
    
    @Override
    public String getName() {
        return "cd";
    }

    Context context = new Context();
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String getManual() {
      String instructions = "The cd command stands for 'change directory', you can use '..' to go to the parent folder, or the name of any folder in the current working directory to enter them. Likewise, you can use the absolute path of a folder.";
      return instructions;
    }
}
