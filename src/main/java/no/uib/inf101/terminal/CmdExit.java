package no.uib.inf101.terminal;

public class CmdExit implements Command {
    @Override
    public String run(String[] args) {
        System.exit(0);
        return "exiting";
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getManual() {
      String instructions = "The exit command exits the shell with the keyword 'exit'.";
      return instructions;
    }
}
