package no.uib.inf101.terminal;

import java.io.File;

public class CmdLs implements Command {
    @Override
    public String run(String[] args) {
    File cwd = this.context.getCwd();
    String s = "";
    for (File file : cwd.listFiles()) {
      s += file.getName();
      s += " ";
    }
    return s;
    }

    @Override
    public String getName() {
        return "ls";
    }
    
    Context context = new Context();
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String getManual() {
      String instructions = "The 'ls' command prints out a list of the files and folders in your current working directory.";
      return instructions;
    }
}
