package no.uib.inf101.terminal;

import java.util.HashMap;

public class CmdMan implements Command {
    @Override
    public String run(String[] args) {
      Command command = comcont.get(args[1]);
      return command.getManual();
    }
    
    @Override
    public String getName() {
        return "cd";
    }

    Context context = new Context();
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    HashMap <String, Command> comcont = new HashMap<>();
    public void setCommandContext(HashMap <String, Command> comcont) {
      this.comcont = comcont;
    }

    @Override
    public String getManual() {
      String instructions = "The man command stands for 'manual', and gives you a manual on how to use a command.";
      return instructions;
    }
}
