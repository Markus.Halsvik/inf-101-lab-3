package no.uib.inf101.terminal;

public class CmdPwd implements Command {
    @Override
    public String run(String[] args) {
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return "pwd";
    }

    Context context = new Context();
    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String getManual() {
      String instructions = "The 'pwd' command prints your current working directory.";
      return instructions;
    }
}
